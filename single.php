<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package superacion
 */

get_header();
if ( function_exists( 'sharing_display' ) ) remove_filter( 'the_content', 'sharing_display', 19 );
if ( function_exists( 'sharing_display' ) ) remove_filter( 'the_excerpt', 'sharing_display', 19 );
?>

	<div id="primary" class="detalle_articulo content-area">

        <div class="portada_detallearticulo">
            <div class="small-12 medium-6 large-6 columns contenedores primera">
                <div class="info">
                    <h1 class="titulo">únete a la</h1>
                    <h4 class="subtitulo">Comunidad</h4>
                    <h1 class="titulo2">de lideres </h1>
                </div>
            </div>
            <div class="small-12 medium-6 large-6 columns contenedores">
                <div class="info">
                    <?php echo do_shortcode('[benchmark-email-lite widget_id="2"]'); ?>
                </div>
            </div>
        </div>

		<div id="main" class="row contenido_post site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_format() );

                    echo '<p class="comparte">Comparte en:</p>'; ?>

            <div class="social">
                <?php
                if ( function_exists( 'sharing_display' ) ) {
                        sharing_display( '', true );
                    }

                    if ( class_exists( 'Jetpack_Likes' ) ) {
                       $custom_likes = new Jetpack_Likes;
                       echo $custom_likes->post_likes( '' );
                    }
                ?>
            </div>
            <?php
			//the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</div><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
