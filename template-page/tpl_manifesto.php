<?php
/*
* Template Name: Manifiesto
*/
get_header();
?>
<div class="manifiesto">
    <section class="portada">
        <div class="small-12 medium-6 large-6 columns contenedores">
            <div class="info">
                <h1 class="titulo">únete a la</h1>
                <h4 class="subtitulo">Comunidad</h4>
                <h1 class="titulo2">de líderes </h1>
            </div>
        </div>
        <div class="small-12 medium-6 large-6 columns contenedores">
            <div class="info">
                <?php echo do_shortcode('[benchmark-email-lite widget_id="2"]'); ?>
            </div>
        </div>
    </section>
    <section class="contenido">
        <div class="row">
        <h1><?php echo get_the_title(); ?></h1>
            <div class="info">
                <?php  while(have_posts()) : the_post(); ?>
                    <?php the_content(); ?>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
</div>
<?php get_footer(); ?>