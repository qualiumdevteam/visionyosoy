<?php
/*
* Template Name: Home
*/
get_header();

$paged = (get_query_var('page')) ? get_query_var('page') : 1;
$args = array(
    'post_type' => 'post',
    'posts_per_page' => 5,
    'paged' => $paged,
    'order' => 'ASC'
);
$query = new WP_Query($args);
?>
<div class="home">
    <section class="portada">
        <div class="small-12 medium-6 large-6 columns contenedores primera">
            <div class="info">
                <h1 class="titulo">únete a la</h1>
                <h4 class="subtitulo">Comunidad</h4>
                <h1 class="titulo2">de líderes </h1>
            </div>
        </div>
        <div class="small-12 medium-6 large-6 columns contenedores">
            <div class="info">
                <?php echo do_shortcode('[benchmark-email-lite widget_id="2"]'); ?>
            </div>
        </div>
    </section>
    <section class="bloghome">
        <?php
        if($query->have_posts()){
            $contador=0;
        while($query->have_posts()) : $query->the_post();
        $feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );?>
            <?php  if($contador%2){ ?>

                <?php if($paged==1 && $contador==3){ ?>

                    <div class="inpar2 large-12 columns contenido_articulo">
                        <div class="large-6 columns info_articulo">
                            <h2>Conferencias</h2>
                            <h4>Sobre</h4>
                            <h1>Liderazgo</h1>
                            <div class="contenedor_btn"><a class="ver_mas" href="<?php echo site_url(); ?>/conferencias">Ver mas</a></div>
                        </div>
                        <div class="large-6 columns imgdestacada">
                            <div style="background-image: url('<?php echo  get_template_directory_uri(); ?>/img/conferencias.jpg')"></div>
                        </div>
                    </div>

                    <div class="par large-12 columns contenido_articulo">
                        <div class="large-6 columns imgdestacada">
                            <div style="background-image: url('<?php echo $feat_image; ?>')"></div>
                        </div>

                        <div class="large-6 columns info_articulo">
                            <h3><?php echo get_the_title(); ?></h3>
                            <p><?php echo strip_tags(substr(get_the_content(),0,200)); ?></p>
                            <div class="social">
                                <?php
                                if ( function_exists( 'sharing_display' ) ) {
                                    sharing_display( '', true );
                                }

                                if ( class_exists( 'Jetpack_Likes' ) ) {
                                    $custom_likes = new Jetpack_Likes;
                                    echo $custom_likes->post_likes( '' );
                                }
                                ?>
                            </div>
                            <div id="btn_generado" style="margin-left: 30px;margin-right: 0px;" class="contenedor_btn"><a class="ver_mas" href="<?php echo get_permalink(); ?>">Ver mas</a></div>
                        </div>
                    </div>
                <?php $contador=0; ?>
                <?php } else{ ?>

                <div class="inpar large-12 columns contenido_articulo">
                    <div class="large-6 columns info_articulo">
                        <h3><?php echo get_the_title(); ?></h3>
                        <p><?php echo strip_tags(substr(get_the_content(),0,200)); ?></p>
                        <div class="social">
                            <?php
                            if ( function_exists( 'sharing_display' ) ) {
                                sharing_display( '', true );
                            }

                            if ( class_exists( 'Jetpack_Likes' ) ) {
                                $custom_likes = new Jetpack_Likes;
                                echo $custom_likes->post_likes( '' );
                            }
                            ?>
                        </div>
                        <div class="contenedor_btn"><a class="ver_mas" href="<?php echo get_permalink(); ?>">Ver mas</a></div>
                    </div>
                    <div class="large-6 columns imgdestacada">
                        <div style="background-image: url('<?php echo $feat_image; ?>')"></div>
                    </div>
                </div>
                    <?php } ?>
            <?php } else{ ?>
                <div class="par large-12 columns contenido_articulo">

                    <div class="large-6 columns imgdestacada">
                        <div style="background-image: url('<?php echo $feat_image; ?>')"></div>
                    </div>
                    <div class="large-6 columns info_articulo">
                        <h3><?php echo get_the_title(); ?></h3>
                        <p><?php echo strip_tags(substr(get_the_content(),0,300)); ?>...</p>
                        <div class="contenedor_btn"><a class="ver_mas" href="<?php echo get_permalink(); ?>">Ver mas</a></div>
                        <div class="social">
                            <?php
                            if ( function_exists( 'sharing_display' ) ) {
                                sharing_display( '', true );
                            }

                            if ( class_exists( 'Jetpack_Likes' ) ) {
                                $custom_likes = new Jetpack_Likes;
                                echo $custom_likes->post_likes( '' );
                            }
                            ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php $contador++; ?>
        <?php  endwhile; }  ?>
        <div class="clearfix"></div>
        <div class="paginav text-center"><span class="info_page">Página <?php echo $paged ?> de <?php echo $query->max_num_pages; ?> </span> <div class="number_page"><?php get_pagination($query); ?></div></div>
    </section>
</div>
<?php get_footer(); ?>