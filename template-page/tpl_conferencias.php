<?php
/*
* Template Name: Conferencias
*/
get_header();
?>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
<div class="conferencias">
    <section class="portada">
        <div class="large-offset-6 small-12 medium-12 large-6 columns contenedores">
            <div class="info">
                <h1 class="titulo">CONFERENCIAS</h1>
                <p class="descipcion">Soy un estratega vocacional, centrado en ayudar a las personas a emprender su misión en el mundo con genuino liderazgo y la gestión inteligente de sus proyectos. Imparto conferencias sobre liderazgo y discernimiento vocacional, establecimiento de la misión personal y el desarrollo  de un liderazgo trascendente.</p>
            </div>
        </div>
    </section>
    <section class="beneficios">
        <h1 class="titulo">Beneficios</h1>
        <?php  while(have_posts()) : the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; ?>
    </section>
    <section class="video">
        <?php $url_video =get_post_meta(get_the_id(),'video_url', true) ?>
        <?php echo do_shortcode('[youtube '.$url_video.' ]') ?>
    </section>
    <section class="testimonios">
        <h1 class="titulo">Testimonios</h1>
        <div class="owl-carousel">
            <?php echo get_post_meta(get_the_id(),'testimonios', true) ?>
        </div>
    </section>
    <section class="contacto">
        <div class="row">
            <h1 class="titulo">Mas información</h1>
            <p class="descripcion">Para recibir más información sobre las conferencias, por favor, ingrese sus datos y nos pondremos en contacto con usted en la prontitud.</p>
            <?php echo do_shortcode('[contact-form-7 id="42" title="form conferencias"]') ?>
        </div>
    </section>
</div>
<?php get_footer(); ?>