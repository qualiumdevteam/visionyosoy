<?php
/*
* Template Name: Contacto
*/
get_header();
?>
<div class="contacto_template">
    <div class="contenedor">
        <h1 class="titulo_contacto">Contacto</h1>
        <p class="descripcion">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore dolor sit amet sed do.</p>
        <div><?php echo do_shortcode('[contact-form-7 id="38" title="Formulario de contacto 1"]'); ?></div>
    </div>
</div>
<?php get_footer(); ?>