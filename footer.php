<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package superacion
 */

?>

	</div><!-- #content -->

    <div class="clearfix"></div>
	<footer id="colophon" class="small-12 medium-12 large-12 site-footer" role="contentinfo">
		<div class="small-12 medium-6 large-6 columns aviso_privacidad"><a href="<?php echo site_url(); ?>/aviso-de-privacidad/"> Aviso de privacidad</a></div>
        <div class="small-12 medium-6 large-6 columns redes left">
            <a target="_blank" href="https://www.facebook.com/angel.vargas.7927?fref=ts"><div class="glyph-icon flaticon-facebook3"></div></a>
            <a target="_blank" href="https://www.instagram.com/yosoyadvs/"><div class="glyph-icon flaticon-instagram19"></div></a>
            <a target="_blank" href="https://twitter.com/@yosoyadvs"><div class="glyph-icon flaticon-logo22"></div></a>
        </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
