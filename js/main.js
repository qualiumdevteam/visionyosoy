jQuery(document).ready(function(){
    jQuery('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        URLhashListener:false,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:2
            }
        }
    })

    jQuery('.benchmarkemaillite-subscribe').find('input[type="text"]').val('');
    jQuery('.benchmarkemaillite-subscribe').find('input[type="text"]').attr("placeholder", "Correo electrónico");

    jQuery('.navimg').click(function(){
        jQuery('#primary-menu').toggleClass('show');
    })
});